let country = prompt('choose coutry:');
country = country.trim();
country = country.toLocaleLowerCase();

let price;
let choosen_country;
let unknown_country = false;

switch (country) {
  case 'китай':
    country = 'Китай';
    price = 100;
    break;
  case 'чили':
    country = 'Чили';
    price = 250;
    break;
  case 'австралия':
    country = 'Австралия';
    price = 170;
    break;
  case 'индия':
    country = 'Индия';
    price = 80;
    break;
  case 'ямайка':
    country = 'Ямайка';
    price = 120;
    break;
  default:
    unknown_country = true;
}

if (true === unknown_country) {
  alert('В вашей стране доставка не доступна');
} else {
  alert(`Доставка в ${country} будет стоить ${price} кредитов`);
}
