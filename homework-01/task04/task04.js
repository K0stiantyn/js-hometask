let credits = 23580;
const PRICE_PER_DROID = 3000;
let message;

let userAnswer = prompt('Скільки дроїдів ви бажаєте придбати?');
if (userAnswer == null) {
  message = 'Відміено користувачем!';
} else {
  let quanitity = parseInt(userAnswer);

  if (quanitity * PRICE_PER_DROID > credits) {
    message = 'Недостатньо коштів на рахунку!';
  } else {
    message = `Ви придбали ${quanitity} дроїдів, на рахунку залишилось ${
      credits - quanitity * PRICE_PER_DROID
    } кредитів`;
  }
  console.log(message);
  alert(message);
}
